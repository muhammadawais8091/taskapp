import React, {FC} from 'react'
import { Card, Form } from 'react-bootstrap';
import {  TaskItemInterface } from '../Interface/TaskItemInterface';
 

const TaskItem: FC<TaskItemInterface>  = ({task, handleClick}) => {   
    return (    
        <Card>                 
            <Card.Body>
                <div className="d-flex">
                    {handleClick&& <Form.Check className="pl-2" type="checkbox" onClick={() => handleClick(task.id) }/>}                      
                    <span>{task.text}</span>
                </div>                    
            </Card.Body>
        </Card>
        
    )
}
export default TaskItem
