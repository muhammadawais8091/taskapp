import 'bootstrap/dist/css/bootstrap.min.css';
import './Assets/css/app.css'
import { Routes, Route} from 'react-router-dom';
import Header from './Shared/Header';
import AddTask from './Components/AddTask'; 
import TaskLists from './Components/TaskLists'; 
import BulkDelete from './Components/BulkDelete'; 
function App() {
  return (
    <div className="App">
        <Header />
        <div className="py-5">
          <Routes>            
              <Route path="/add-task" element={<AddTask />} /> 
              <Route path="/bulk-delete" element={<BulkDelete />} /> 
              <Route path="/list-tasks" element={<TaskLists />} /> 
              <Route path="/" element={<TaskLists />} />   
          </Routes>
        </div>
    </div>
  );
}
{/* <Route path="/add-task" element={<AddTask />} />
<Route path="/task-lists" element={<TaskLists />} />   
<Route path="/bulk-delete" element={<BulkDelete />} />  
<Route path="/task-lists" element={<TaskLists />} />   */}

export default App;
