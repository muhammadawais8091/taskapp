export interface TaskInterface {
    id: string;
    text: string;
    isChecked: boolean;
}