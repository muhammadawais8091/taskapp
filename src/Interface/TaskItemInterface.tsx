import  { TaskInterface } from './TaskInterface'
export interface TaskItemInterface {
    handleClick?: (id: string) =>void;
    task: TaskInterface;
}