import { Link } from 'react-router-dom'
const Header = () => {
    return (
        <header className="header">
            <div className="header-wrap">
                 <div className="header-nav">                     
                        <nav className="navbar  ">                              
                             <ul className="navbar-nav">                                     
                                <li className="nav-item">
                                    <Link className="nav-link" to="/list-tasks">Task Lists</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/add-task">Add Task</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="bulk-delete">Bulk Delete</Link>
                                </li>
                            </ul>
                        </nav>
                 </div>
            </div>
        </header>
    )
}
export default Header;