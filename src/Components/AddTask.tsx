import React, { FC, ChangeEvent, useEffect} from 'react';
import { useNavigate } from 'react-router-dom'
import {Card, Container, Form, Button, Row, Col } from 'react-bootstrap';
import { TaskInterface } from '../Interface/TaskInterface';
 const AddTask:FC = () => {
    const navigate = useNavigate();
    const [taskName, setTaskName] = React.useState<string>("");
    const [taskList, setTaskList] = React.useState<TaskInterface[]>([]);
    const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
        setTaskName(event.target.value)              
    };
    const addTask = () => {
        if(taskName == ""){
                return
        }
        const task : TaskInterface  = {
            id : String(new Date().getTime()),
            text : taskName,
            isChecked: false
        }      
        let innerList = [...taskList, {...task}];
        setTaskList([...innerList]);
        console.log(innerList);
        localStorage.removeItem("taskList");
        localStorage.setItem("taskList", JSON.stringify(innerList));
        navigate('/list-tasks')
    };
    useEffect(() => {
        let myTaskList = localStorage.getItem("taskList");
        if(myTaskList){
            let myTaskListInner = JSON.parse(myTaskList)
            setTaskList([...myTaskListInner])
        }                 
    }, [])
    
    return (
         <Container>
              <Row className="justify-content-center">
                  <Col md={7}>
                       <Card>
                           <Card.Body>
                                <Form>                 
                                    <Form.Group className="mb-3" controlId="formBasicPassword">
                                        <Form.Label>Task</Form.Label>
                                        <Form.Control type="text" placeholder="Name" onChange={handleChange} />
                                    </Form.Group>                                    
                                    <Button variant="primary" type="button" onClick={addTask}>
                                        Add Task
                                    </Button>
                                </Form>
                           </Card.Body>
                       </Card>
                  </Col>
              </Row>
         </Container>
    )
}
export default AddTask
