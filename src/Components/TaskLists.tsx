import React, { FC, useState, useEffect} from 'react';
import { Container,  Row, Col } from 'react-bootstrap';
import Task from '../Fragments/TaskItem';
import { TaskInterface } from '../Interface/TaskInterface';

const TaskList: FC = () => {
    const [taskList, setTaskList] = useState<TaskInterface[]>([]);
    useEffect(() => {
        let myTaskList= localStorage.getItem("taskList");
        if(myTaskList){
            let myTaskListInner = JSON.parse(myTaskList)
            setTaskList([...myTaskListInner])
        }                 
    }, []);
    return (
        <Container>
           <Row className="justify-content-center">
             <Col md={7}>               
                {
                    taskList.length > 0 ?
                    <>
                        <ul className="taskList list-unstyled">
                            {
                                taskList.map((task:TaskInterface)=>{
                                    return <li  key={task.id}><Task  task={task}/></li>
                                })
                            }
                        </ul>                            
                    </> : <h5 className="text-center">You have no task in your list.</h5>
                }
             </Col>
           </Row>
        </Container>    
    )
}

export  default TaskList