import React, { FC, useState, useEffect, MouseEvent} from 'react';
import { Container,  Row, Col } from 'react-bootstrap';
import Task from '../Fragments/TaskItem';
import { TaskInterface } from '../Interface/TaskInterface';
const BulkDelete: FC = () => {
    const [taskList, setTaskList] = useState<TaskInterface[]>([]);
    const handleClick= (id: string):void => {
        let itemId= id;
        const itemList = [...taskList];
        let item = itemList.find((item)=>item.id==itemId);
        const itemIndex =  itemList.findIndex((item)=>item.id==itemId);
        itemList[itemIndex].isChecked = !item?.isChecked
        localStorage.setItem("taskList", JSON.stringify(itemList));
        setTaskList([...taskList])
    }
    useEffect(() => {
        let myTaskList = localStorage.getItem("taskList");
        if(myTaskList){
            let myTaskListInner = JSON.parse(myTaskList)
            setTaskList([...myTaskListInner])
        }                 
    }, []);
    const bulkDelete = (event: MouseEvent<HTMLButtonElement>) =>{
        event.preventDefault();
        let filterItems =  taskList.filter((task)=>task.isChecked === false);
        console.log(filterItems);
        localStorage.setItem("taskList", JSON.stringify(filterItems));
        setTaskList([...filterItems])   
    }
    return (
        <Container>
             <Row className="justify-content-center">
                <Col md={7}>
                    {
                        taskList.length > 0 ?
                        <>
                            <ul className="taskList list-unstyled">
                                {
                                    taskList.map((task:TaskInterface)=>{
                                        return <li key={task.id} ><Task   handleClick={ handleClick} task={task}/></li>
                                    })
                                }
                            </ul>
                            <button type="button" className="btn btn-danger" onClick={bulkDelete}> Delete</button>
                        </>  : <h5 className="text-center">You have no task in your list.</h5>
                    }
               </Col>
           </Row>
        </Container>    
    )
}

export  default BulkDelete